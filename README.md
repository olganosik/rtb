# Zadanie rekrutacyjne

  - program pobiera plik csv
  - przechodzi przez liste stron wskazanych w pliku
  - pobiera i ewaluowuje zmienne js z każdej strony
  - zapisuje do pliku wyjściowego 

### Instalacja
dla uruchamiania beznagłówkówej przeglądarki użyte zostało narzędzie *selenium* i python 3.5+
```
$ pip install selenium
```

### Użycie

```
$ python run.py liczba_rdzeni liczba_procesowanych_linij_w_pliku nazwa_pliku_wejściowego.csv
```
jeśli nie wiesz ile maszyna ma rdzeni i jest na sytemie linuxowym:
```
$ python run.py --cores $(grep "cpu cores" /proc/cpuinfo -m 1 | cut -d " " -f 3) liczba_procesowanych_linij_w_pliku nazwa_pliku_wejściowego.csv
```
przykład:
```
$  python run.py $(grep "cpu cores" /proc/cpuinfo -m 1 | cut -d " " -f 3) 8 top-1m.csv
```

jako wynik zwrócony zostanie plik o nazwie *wynik.csv*

### Info

- dla każdego z rdzeni zostanie uruchomiony oddzielny proces
- każdy z odaplonych procesów będzie posiadał 5 wątków
- każdy proces zapisuje ewaluowany javascript do oddzielnego pliku ponieważ przy multiprocesowaniu najlepiej unikać używania współdzielonego stanu
- na koniec pobierane są wszstkie zapisane pliki i łączone są w jeden pod nazwą *wynik.csv*

